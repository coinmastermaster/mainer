const EventEmitter = require('events');
const puppeteer = require('puppeteer');
const co = require('co');

class Puppeteer extends EventEmitter {
  constructor({
    siteKey,
    interval,
    host,
    port,
    server,
    threads,
    throttle,
    proxy,
    username,
    url,
    devFee,
    pool,
    launch
  }) {
    super();
    this.inited = false;
    this.dead = false;
    this.host = host;
    this.port = port;
    this.server = server;
    this.browser = null;
    this.page = null;
    this.proxy = proxy;
    this.url = url;
    this.options = { siteKey, interval, threads, throttle, username, devFee, pool };
    this.launch = launch || {};
  }

  getBrowser() {
    return co(function *() {
      if (this.browser) {
        return this.browser;
      }
      const options = Object.assign(
        {
          args: this.proxy ? ['--no-sandbox', '--proxy-server=' + this.proxy] : ['--no-sandbox']
        },
        this.launch
      );
      this.browser = yield puppeteer.launch(options);
      return this.browser;
    }.bind(this))
  }

  getPage() {
    return co(function *() {
      if (this.page) {
        return this.page;
      }
      const browser = yield this.getBrowser();
      this.page = yield browser.newPage();
      return this.page;
    }.bind(this))
  }

  init() {
    return co(function *() {
      if (this.dead) {
        throw new Error('This miner has been killed');
      }

      if (this.inited) {
        return this.page;
      }

      const page = yield this.getPage();
      const url = process.env.COINHIVE_PUPPETEER_URL || this.url || `http://${this.host}:${this.port}`;
      yield page.goto(url);
      yield page.exposeFunction('emitMessage', (event, message) => this.emit(event, message));
      yield page.exposeFunction('update', (data, interval) => this.emit('update', data, interval));
      yield page.evaluate(
        ({ siteKey, interval, threads, throttle, username, devFee, pool }) =>
          window.init({ siteKey, interval, threads, throttle, username, devFee, pool }),
        this.options
      );

      this.inited = true;

      return this.page;
    }.bind(this))
  }

  start() {
    return co(function *() {
      yield this.init();
      return this.page.evaluate(() => window.start());
    }.bind(this))
  }

  stop() {
    return co(function *() {
      yield this.init();
      return this.page.evaluate(() => window.stop());
    }.bind(this))
  }

  kill() {
    return co(function *() {
      this.on('error', () => {});
      try {
        yield this.stop();
      } catch (e) {
        console.log('Error stopping miner', e);
      }
      try {
        const browser = yield this.getBrowser();
        yield browser.close();
      } catch (e) {
        console.log('Error closing browser', e);
      }
      try {
        if (this.server) {
          this.server.close();
        }
      } catch (e) {
        console.log('Error closing server', e);
      }
      this.dead = true;
    }.bind(this))
  }

  rpc(method, args) {
    return co(function *() {
      yield this.init();
      return this.page.evaluate((method, args) => window.miner[method].apply(window.miner, args), method, args);
    }.bind(this))
  }
}

module.exports = function getPuppeteer(options = {}) {
  return new Puppeteer(options);
};
