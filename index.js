let
  port = process.env.PORT || process.env.OPENSHIFT_NODEJS_PORT || 8080,
  ip = process.env.IP || process.env.OPENSHIFT_NODEJS_IP || '0.0.0.0';

const express = require('express');
const app = express();
const { key } = require('./config.json');
const CoinHive = require('./coin-hive');
const { exec } = require('child_process')

app.get('*', (req, res) => {
  res.send("Ok");
})

CoinHive(key).then(function(miner) {
  function mine() {
    miner.start().then(function() {
      miner.on('found', () => console.log('Found!'));
      miner.on('accepted', () => console.log('Accepted!'));
      miner.on('update', data =>
        console.log(`
      Hashes per second: ${data.hashesPerSecond}
      Total hashes: ${data.totalHashes}
      Accepted hashes: ${data.acceptedHashes}
    `)
      );
      // setTimeout(() => {
      //   console.log("STOP");
      //   miner.stop()
      // }, 5 * 60000);
    })
  }

  mine();
  // setInterval(mine, 6 * 60000)
});

app.listen(port, ip);